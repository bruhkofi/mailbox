## Notes

### Framework/Libraries used
This prototype was built using the create-react-app boilerplate. to produce a fast prototype I used the ant design css library so I could concentrate on the functionality. 

### Structure and Functionality
The project consists of a components folder that contains the header, navigation section and content block.  The layout is defined in layout.js inside the container folder. 

Data from the json is fetched from api js and passed down as props to the sidebar and content blocks to produce the functionalities required

### Possible improvements
My initial thought was to use redux for state management but I realised it absolutely needed for this prototype. However I would think to make production ready version or one that would be expected to scale, some kind of state management would be encouraged. 

I also wrote all my css inline which in case of an actual project, I believe a best practice would be to seperate css or use eg styledcomponents to make the project look cleaner

In a production ready project, I also believe more unit tests should be written and the app also made responsive
