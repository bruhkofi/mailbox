import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
const { Footer, Content, Header } = Layout;

class MainLayout extends Component {
    render() {
        return (
            <Layout style={{ height: '100vh' }}>
                <Header className="header">
                    <div className="logo" />
                    <Menu theme="dark" mode="horizontal" style={{ lineHeight: '64px' }}>
                        <div>MailBox</div>
                    </Menu>
                </Header>
                {
                    <Content style={{ padding: '50px 50px' }}>
                        <Layout style={{ padding: '24px 0', background: '#fff', height: '80vh' }}>
                            {this.props.children}
                        </Layout>
                    </Content>
                }
            </Layout>
        );
    }
}

export default MainLayout;
