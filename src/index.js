import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import { getData } from './services/api';


ReactDOM.render(<App messages={getData()} />, document.getElementById('root'));
