import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Menu, Icon, Layout, List, Avatar, Card } from 'antd';
import data from '../services/data';
import dateFns from 'date-fns';
import { getData } from '../services/api';

const { SubMenu } = Menu;
const { Sider } = Layout;

const Sidebar = ({ messages, onSelectMessage, onDeleteMessage, selectedMessageId }) => (
    <Sider width={200} style={{ background: '#fff' }}>
        <Menu mode="inline" defaultSelectedKeys={['1']} defaultOpenKeys={['sub1']} style={{ height: '100%' }}>
            <SubMenu
                key="sub1"
                title={
                    <span>
                        <Icon type="mail" />
                        Inbox
                    </span>
                }
                style={{ height: '70vh', overflowX: 'scroll' }}
            >
                <List
                    itemLayout="horizontal"
                    dataSource={messages}
                    style={{ padding: '10px' }}
                    renderItem={message => (
                        <List.Item
                            style={{
                                fontWeight:
                                    selectedMessageId === message.uid
                                        ? 'normal'
                                        : message.readStatus ? 'lighter' : 'bold'
                            }}
                        >
                            <List.Item.Meta
                                title={
                                    <div>
                                        <div
                                            style={{ postition: 'relative', float: 'right', cursor: 'pointer' }}
                                            onClick={() => onDeleteMessage(message.uid)}
                                        >
                                            X
                                        </div>
                                        <div>{message.sender}</div>
                                    </div>
                                }
                                description={
                                    <div onClick={() => onSelectMessage(message.uid)}>
                                        <div>{message.subject}</div>
                                        <div>{dateFns.format(message.time_sent * 1000, 'ddd DD MMMM, hh:mm')}</div>
                                    </div>
                                }
                            />
                        </List.Item>
                    )}
                />
            </SubMenu>
        </Menu>
    </Sider>
);

Sidebar.propTypes = {
    messages: PropTypes.array.isRequired,
    onSelectMessage: PropTypes.func.isRequired,
    onDeleteMessage: PropTypes.func.isRequired
};

export default Sidebar;
