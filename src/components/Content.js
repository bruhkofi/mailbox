import React, { Component } from 'react';
import { Layout } from 'antd';
import { getData } from '../services/api';
const { Content } = Layout;

const ContentBlock = ({ message }) => (
    <Content style={{ padding: '0 24px', minHeight: 280 }}>{message ? message.message : 'Please select a mail to view its contents'}</Content>
);

export default ContentBlock;
