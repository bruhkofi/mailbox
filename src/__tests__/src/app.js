import React from 'react';
import renderer from 'react-test-renderer';
import App from '../../app';

describe('Application Test', () => {
    it('renders without crashing', () => {
        const component = renderer.create(<App messages={[{ uid: '1234', message: 'Come get me!' }]} />);
        expect(component.toJSON()).toMatchSnapshot();
    });
});
