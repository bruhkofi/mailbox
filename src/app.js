import React from 'react';
import './index.css';
import MainLayout from './container/Layout';
import ContentBlock from './components/Content';
import Sidebar from './components/Sidebar';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: props.messages.map(message => ({ ...message, deleted: false, readStatus: false })),
            selectedMessageId: ''
        };
    }

    selectMessage = uid => {
        this.setState({
            selectedMessageId: uid,
            messages: this.state.messages.map(message => {
                if (message.uid === uid) {
                    return { ...message, readStatus: true, selected: true };
                }

                return { ...message, selected: false };
            })
        });
    };

    deleteMessage = uid => {
        this.setState({
            messages: this.state.messages.map(message => {
                if (message.uid === uid) {
                    return { ...message, deleted: true };
                }

                return message;
            }),
            selectedMessageId: uid === this.state.selectedMessageId ? '' : this.state.selectedMessageId
        });
    };

    render() {
        return (
            <MainLayout>
                <Sidebar
                    onSelectMessage={this.selectMessage}
                    onDeleteMessage={this.deleteMessage}
                    selectedMessageId={this.state.selectedMessageId}
                    messages={this.state.messages.filter(message => !message.deleted)}
                />
                <ContentBlock
                    message={this.state.messages.find(message => message.uid === this.state.selectedMessageId)}
                />
            </MainLayout>
        );
    }
}

export default App;
